package com.crossworkers.test.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.crossworkers.test.YahooCurrencyConverter;
import com.crossworkers.test.entity.ConverterEntity;

@RestController
public class ConverterController {

    @RequestMapping(value="/currency-converter/", method=RequestMethod.POST)
    public @ResponseBody String convert(@RequestParam Map<String,String> requestParams) {
    	
    	double amount=Double.parseDouble(requestParams.get("amount"));
    	
    	String fromCurrency=requestParams.get("fromCurrency");
    	fromCurrency=fromCurrency!=null && !fromCurrency.equals("") ?fromCurrency : "USD";
    	
    	
    	String toCurrency=requestParams.get("toCurrency");
    	toCurrency=toCurrency!=null && !toCurrency.equals("") ? toCurrency : "EUR";
    	
    	float current=0;
    	
    	YahooCurrencyConverter ycc = new YahooCurrencyConverter();
    	
    	try {
            current = ycc.convert(fromCurrency, toCurrency);
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    	
    	return String.valueOf((current*amount) + " " +toCurrency);
    	
    }
}
