package com.crossworkers.test;

import java.io.IOException;

public interface CurrencyConverter {
	 public float convert(String currencyFrom, String currencyTo) throws IOException;
}
